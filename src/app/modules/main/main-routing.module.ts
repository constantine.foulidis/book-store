import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookstoreBooksViewComponent } from './components/bookstore-books-view/bookstore-books-view.component';
import { BookstoreViewComponent } from './components/bookstore-view/bookstore-view.component';
import { ContactViewComponent } from './components/contact-view/contact-view.component';
import { MainComponent } from './components/main/main.component';
import { MainContainerComponent } from './containers/main-container/main-container.component';

const routes: Routes = [{
  path: "",
  component: MainContainerComponent,
  children: [
     { path: "main", component: MainComponent },
    //  { path: 'profile', loadChildren: () => import('../../profile/profile.module').then(m => m.ProfileModule) },

    { path: "bookstore", component: BookstoreViewComponent },
    { path: "books", component: BookstoreBooksViewComponent },
    { path: "contact-us", component: ContactViewComponent },
    { path: "", redirectTo: "main", pathMatch: "full" },
  ],
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
