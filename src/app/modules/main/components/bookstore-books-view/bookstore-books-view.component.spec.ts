import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookstoreBooksViewComponent } from './bookstore-books-view.component';

describe('BookstoreBooksViewComponent', () => {
  let component: BookstoreBooksViewComponent;
  let fixture: ComponentFixture<BookstoreBooksViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookstoreBooksViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookstoreBooksViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
