import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BookStoreHistoryComponent } from './book-store-history.component';

describe('BookStoreHistoryComponent', () => {
  let component: BookStoreHistoryComponent;
  let fixture: ComponentFixture<BookStoreHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BookStoreHistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStoreHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
