import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainContainerComponent } from './containers/main-container/main-container.component';
import { MainComponent } from './components/main/main.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MainHeroComponent } from './components/main-hero/main-hero.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { BookstoreViewComponent } from './components/bookstore-view/bookstore-view.component';
import { BookstoreBooksViewComponent } from './components/bookstore-books-view/bookstore-books-view.component';
import { ContactViewComponent } from './components/contact-view/contact-view.component';
import { SubHeroComponent } from './components/sub-hero/sub-hero.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { BookStoreHistoryComponent } from './components/book-store-history/book-store-history.component';
import { GetInTouchComponent } from './components/get-in-touch/get-in-touch.component';



@NgModule({
  declarations: [
    MainContainerComponent,
    MainComponent,
    MainHeroComponent,
    NavBarComponent,
    BookstoreViewComponent,
    BookstoreBooksViewComponent,
    ContactViewComponent,
    SubHeroComponent,
    AboutUsComponent,
    BookStoreHistoryComponent,
    GetInTouchComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
  ]
})
export class MainModule { }
