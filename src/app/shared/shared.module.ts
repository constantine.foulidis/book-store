import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroNavComponent } from './hero-nav/hero-nav.component';
import {  RouterModule } from '@angular/router';
import { MainHeroNavComponent } from './main-hero-nav/main-hero-nav.component';
import { FooterComponent } from './footer/footer.component';
import { BookCardComponent } from './book-card/book-card.component';
import { SearchComponent } from './search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { CategorySelectComponent } from './category-select/category-select.component';
import {MatSelectModule} from '@angular/material/select';




@NgModule({
  declarations: [
    HeroNavComponent,
    MainHeroNavComponent,
    FooterComponent,
    BookCardComponent,
    SearchComponent,
    CategorySelectComponent
  ],
  exports: [
    HeroNavComponent,
    FooterComponent,
    SearchComponent,
    CategorySelectComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule
    ]
})
export class SharedModule { }
