import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainHeroNavComponent } from './main-hero-nav.component';

describe('MainHeroNavComponent', () => {
  let component: MainHeroNavComponent;
  let fixture: ComponentFixture<MainHeroNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainHeroNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainHeroNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
