import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Output() messageEvent = new EventEmitter<string>();
  searchFormControl = new FormControl('');
  constructor() { }

  ngOnInit(): void {
  }
  inputChange(){
    this.messageEvent.emit(this.searchFormControl.value);
  }

}
